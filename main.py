'''
@author: Henok Million (ATR/1810/08)
'''

import sys

offset, phrase = 0, ''

def accept_input():
    offset = int(input("Please enter offset \n>> "))
    phrase = raw_input("Please enter the phrase you want to encode \n>> ")
    return offset, phrase

if (len(sys.argv) > 1):
    help_ = ['-h', '-help', '--h', '--help']
    if (sys.argv[1] in help_):
        print '''
            Caeasar Cipher:
            
            usage: [:-h] [:offset] [:phrase to encrypt]

            commands
            -h: help
                alrentes: [-help] [--h] [--help]
            
        '''
        exit()
    elif len(sys.argv) == 3:
        offset = int(sys.argv[1])
        phrase = sys.argv[2]
    else:
        offset, phrase = accept_input()
else:
   offset, phrase = accept_input()

nums = [x for x in range(offset + 32, offset + 32 + 91)]
chars = [chr(c) for c in range(32, 123)]

rel = dict(zip(chars, nums))

output = [chr(rel[c]) for c in phrase]
print 'output>> ', ''.join(output)

original = [chr(ord(c) - offset) for c in output]
print 'original>> ', ''.join(original)


