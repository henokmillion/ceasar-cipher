# Ceasar Cipher
An application to encrypt messages.
### Usage
run `$ python main.py <offset> <phrase>`

    - offset is the offset amount
    - phrase is the message to encrypt

or

run `$ python main.py` and follow the prompt

### Help
`$ python main.py -h`

Alternatives:  **--h** | **-help** | **--help**


### Author
Henok Million